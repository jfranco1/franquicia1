package co.com.franquicia1.servicios;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import co.com.franquicia1.dto.Cliente;
import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;

public class GeneradorTarjetaTest {
	
	private GeneradorTarjetas generadorTarjetas;
	
	@Before
	public void iniciarTest() {
		generadorTarjetas = new GeneradorTarjetas();
	}
	
	@Test
	public void testNombre(){
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Franco Quintero");
		cliente.setId("15441218");
		cliente.setNombre("Juan Fernando");
		cliente.setTipoId("CC");
		
		
		DatosGeneracionTarjeta datosGeneracionTarjeta = new DatosGeneracionTarjeta();
		datosGeneracionTarjeta.setBanco("Bancolombia");
		datosGeneracionTarjeta.setCupo(10000);
		datosGeneracionTarjeta.setCliente(cliente);
				
		//Act
		Tarjeta tarjeta = generadorTarjetas.generar(datosGeneracionTarjeta);
		
		//Assert
		assertEquals("FRANCO QUINTERO JUAN FERNANDO",tarjeta.getNombreTarjeta());
	}
	
	
	@Test
	public void testNombre2(){
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Franco Quintero");
		cliente.setId("15441218");
		cliente.setNombre("Juan Fernando");
		cliente.setTipoId("CC");
		
		
		DatosGeneracionTarjeta datosGeneracionTarjeta = new DatosGeneracionTarjeta();
		datosGeneracionTarjeta.setBanco("Bancolombia");
		datosGeneracionTarjeta.setCupo(10000);
		datosGeneracionTarjeta.setCliente(cliente);
				
		//Act
		Tarjeta tarjeta = generadorTarjetas.generar(datosGeneracionTarjeta);
		
		//Assert
		assertEquals("FRANCO QUINTERO JUAN FERNANDO",tarjeta.getNombreTarjeta());
	}
	
	@Test
	public void tesCodigoSeguridad(){
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Franco Quintero");
		cliente.setId("15441218");
		cliente.setNombre("Juan Fernando");
		cliente.setTipoId("CC");
		
		
		DatosGeneracionTarjeta datosGeneracionTarjeta = new DatosGeneracionTarjeta();
		datosGeneracionTarjeta.setBanco("Bancolombia");
		datosGeneracionTarjeta.setCupo(10000);
		datosGeneracionTarjeta.setCliente(cliente);
		
				
		//Act
		Tarjeta tarjeta = generadorTarjetas.generar(datosGeneracionTarjeta);
		
		//Assert
		assertEquals(3,tarjeta.getCodigoSeguridad().length());
	}
	

}
