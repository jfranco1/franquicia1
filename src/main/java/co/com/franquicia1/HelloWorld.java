package co.com.franquicia1;

import javax.jws.WebService;

@WebService
public interface HelloWorld {
    String sayHi(String text);
}

