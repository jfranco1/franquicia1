package co.com.franquicia1.servicios;

import java.util.Random;

import co.com.franquicia1.dto.DatosGeneracionTarjeta;
import co.com.franquicia1.dto.Tarjeta;

public class GeneradorTarjetas {
	Random random = new Random();
	private static final int DIGITO_SEGURIDAD = 3;
	
	public Tarjeta generar(DatosGeneracionTarjeta datos) {
		Tarjeta tarjeta = new Tarjeta();
		tarjeta.setNombreTarjeta(crearNombre(datos));
		String codigo = crearCodigoSeguridad();
		tarjeta.setCodigoSeguridad(codigo);
		return tarjeta;
	}
	
	private String crearNombre (DatosGeneracionTarjeta datos) {
		return  (datos.getCliente().getApellidos()+" "+datos.getCliente().getNombre()).toUpperCase();
	}
	
	private String crearCodigoSeguridad() {
		StringBuilder numero = new StringBuilder();
		for(int i = 1;i<= DIGITO_SEGURIDAD;i++) {
			int numeroRandom = random.nextInt(9);
			numero.append(Integer.toString(numeroRandom));
			
		}
		return numero.toString();
		
	}

}
